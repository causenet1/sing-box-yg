#!/bin/bash
#
# Encrypted by Rangga Fajar Oktariansyah (Anak Gabut Thea)
#
# This file has been encrypted with BZip2 Shell Exec <https://github.com/FajarKim/bz2-shell>
# The filename '1sb.sh' encrypted at Tue Jan 23 05:23:04 UTC 2024
# I try invoking the compressed executable with the original name
# (for programs looking at their name).  We also try to retain the original
# file permissions on the compressed file.  For safety reasons, bzsh will
# not create setuid or setgid shell scripts.
#
# WARNING: the first line of this file must be either : or #!/bin/bash
# The : is required for some old versions of csh.
# On Ultrix, /bin/bash is too buggy, change the first line to: #!/bin/bash5
#
# Don't forget to follow me on <https://github.com/FajarKim>
skip=75

tab='	'
nl='
'
IFS=" $tab$nl"

# Make sure important variables exist if not already defined
# $USER is defined by login(1) which is not always executed (e.g. containers)
# POSIX: https://pubs.opengroup.org/onlinepubs/009695299/utilities/id.html
USER=${USER:-$(id -u -n)}
# $HOME is defined at the time of login, but it could be unset. If it is unset,
# a tilde by itself (~) will not be expanded to the current user's home directory.
# POSIX: https://pubs.opengroup.org/onlinepubs/009696899/basedefs/xbd_chap08.html#tag_08_03
HOME="${HOME:-$(getent passwd $USER 2>/dev/null | cut -d: -f6)}"
# macOS does not have getent, but this works even if $HOME is unset
HOME="${HOME:-$(eval echo ~$USER)}"
umask=`umask`
umask 77

bztmpdir=
trap 'res=$?
  test -n "$bztmpdir" && rm -fr "$bztmpdir"
  (exit $res); exit $res
' 0 1 2 3 5 10 13 15

case $TMPDIR in
  / | */tmp/) test -d "$TMPDIR" && test -w "$TMPDIR" && test -x "$TMPDIR" || TMPDIR=$HOME/.cache/; test -d "$HOME/.cache" && test -w "$HOME/.cache" && test -x "$HOME/.cache" || mkdir "$HOME/.cache";;
  */tmp) TMPDIR=$TMPDIR/; test -d "$TMPDIR" && test -w "$TMPDIR" && test -x "$TMPDIR" || TMPDIR=$HOME/.cache/; test -d "$HOME/.cache" && test -w "$HOME/.cache" && test -x "$HOME/.cache" || mkdir "$HOME/.cache";;
  *:* | *) TMPDIR=$HOME/.cache/; test -d "$HOME/.cache" && test -w "$HOME/.cache" && test -x "$HOME/.cache" || mkdir "$HOME/.cache";;
esac
if type mktemp >/dev/null 2>&1; then
  bztmpdir=`mktemp -d "${TMPDIR}bztmpXXXXXXXXX"`
else
  bztmpdir=${TMPDIR}bztmp$$; mkdir $bztmpdir
fi || { (exit 127); exit 127; }

bztmp=$bztmpdir/$0
case $0 in
-* | */*'
') mkdir -p "$bztmp" && rm -r "$bztmp";;
*/*) bztmp=$bztmpdir/`basename "$0"`;;
esac || { (exit 127); exit 127; }

case `printf 'X\n' | tail -n +1 2>/dev/null` in
X) tail_n=-n;;
*) tail_n=;;
esac
if tail $tail_n +$skip <"$0" | bzip2 -cd > "$bztmp"; then
  umask $umask
  chmod 700 "$bztmp"
  (sleep 5; rm -fr "$bztmpdir") 2>/dev/null &
  "$bztmp" ${1+"$@"}; res=$?
else
  printf >&2 '%s\n' "Cannot decompress ${0##*/}"
  printf >&2 '%s\n' "Report bugs to <fajarrkim@gmail.com>."
  (exit 127); res=127
fi; exit $res
BZh91AY&SYo��� ����v���������������������� `\^��}�g���Ԋ:{{��kC������o�׷=w=]���r:{�<�+ �Kv�;��k�۹�����{��u����m�N^�s��ԯw��}������*��b���Ez$z�0�}��97�n�����5zrW�4�Q��͉+}�{x�R�

nܺ펚�R2�u���r�ܪyZ��:�>�{��{w��N��}צ�oN��n��a+ٯv��Q]�� ���@O�����I!�&�  aJx�i2hjm'��Shi�j��xA�)�ɉ�F�F��� 	A	������*{%7�jm2�)�?Q�oI��='��CO(f��� �  Phh  $A4$5)�S4�Sd�G�L�h�T~���4 ��=M��A��44h 4� @   	4� �&I��꟦��OM�6�����4ѧ���� A�d 2@ h�   "ILS#OS&H�)�j��L��Ѥڞ��m5�zL�SЀi���i����  � z'�� @F��4&����O%?RzOS�)������Mh�42 �h�      r��{�
�=�׃H����l�?��}ia/�_����L���X	��b�9�E0�fA�¾li�8�3ֳ����-� zbݓ�2�4��aKz��A$BD" ��MLI�e1�AX�H� �� ��2�bn��SH��� 9>N��A�1 �h�@�`�%�&���&��#:�P�9��#?�ń"�ER���
2+@q��x-���{�z����y^1b~T���mP���u0�O��<��KJ��Y�L��x�v�d����w�Gb,a��jL&l9`��aL0�03 C<��.�����kP
~��0b'���ޯ	��0s�gϤW��;�Dt��;�M�|a��$�$�r���2�,L�%Dɧ}GYehH�E>�̓�f��;"Tv���ߩ2FLm@�)I�}!xg{��*c�8!�]4��Y����[��^�n�}}R7u>��MbT�[W��J��8v� QH�q�~d���Q|�w�%���ζ3����i�J�T�p<�s��;:��z2m���eI�����4���u��jԚ8�҃/b:�6�c� H6-`4���U٘\@׌��޶�U.�G=�՞�D�k�w(B�Yx������|}��(�lw7�Z1HIky<"�b+]�D�(�]NAt���䠊Kf�g���9��L��ǭ�UY�?.����3���l4��4CH5��IZW*$
_H%��Ap�
��J�2k����r�W�\<��SB��#ISo�\���N�T�oA�� �
jy�[��s��J�7맑G�!�����1S�n�b���f�9�K*褐8���n)n��%�"��g(� {��W.[�nI]��l� �着�8@�QL�I6���GpEZ��D�#vp�M�-��L"��*Q*&�S4:E�U}	�b^�]Ʊ*��A-jR�:xvEÌ0�0Qk�y�)��Y�)�:̋���Wvda�����8�
om17�'eY
�C�y47<l���0\�����Vyvl<N�R7��˗8�rٮ�;�Av���T�)�����\���,Cz[�X�t�t��ެ����U.x�0�g]�8��n�N�o�'#R�#�V�	5�K�݅A�T�����=��VeJ�ܵ9U1������$M���fʹsOjT����_�#_m�)�S�[>�)�lٺ��>ȒdTS�
ʿ�cY�@ǧ�M���QO,p���d�"*�����D}�e�#6�
kG1jb"��V(�2�P����h�bKﳋWx�6�=����9w���fda$��5�����:��%P������KG���s*q;a8h�5󙙝jrf�!P�$p�?��7-�F�4p��m��50`�&��W'մ�-x�t�����f�ZR&�k �IKCb�BɌ�5��V&Cu����\����}���!�$;|�cc+A�FL�H�"�n�7��D���jEPK�(&Ҧ��n��{2�q��QCrԍ�h?���EM:�)G��j��߂�)(����>#F��9�)��h�fHr�hA�F�q�ܮ�L0����!yx�A�]h%ˈ����s�8��E&p��%A����]�<�%�F6%_��q��A��_]���d9V�G)������%��KJS-�0���}��a0��ϰh��ф���E��ZD%<��-�+#8fQi�����7/<X��Kb�y���,3��$��\��ZAc%6|�Pr�Է#e�����IuY"�c4�8U���]ޛ�t�)�Q|sS����p��
'��a��kH	.��DR�9
C�kV�Ì��l�>�4}ȗ��x�8�yT�����ӇG�'C��f��L�(�ڴ�T���Z+�d�0j}qNZH�b'�&�d�(Q��R�ωcȇ:>N ���;�X�O�I# s2���w7N1���rΝxIs���8��+�g�«����� 6����IG����X�g+s??\���
��;���(�y�-|�eG�0��Oo���/�d.@,(%n�A)Oa�ך��=38w����G��4;���]�Ip�@����n�[�nΌ�n^�9�U�^��)��T�.�����m3 UAw*������6���*J������ΏN�ޕO b59�<�M2��Ϊn`k$%&��%��@cm��=�xo�4�-��t��N��������N
-\����(o1��kQt�������!3��*�Hi@�������466%_�b;��̜
�I#�E�T��_�D�y���������bU&^�v"`q�Z�F���E/��,�8�6w�S�y:$�B�"(/ ��Pco�� �����wi�q��6����V��#��� ���$�!�����sH�P�`-P�h]0�Nmx ���d�M��m	6�h=�KG��#�1C�u��4=��w���E��O�M]�p�a�G"7���!�o���=�u۵V�p(��M��3Bd�f=���?(���D��8]�A���\����3�p0Bd)v�aV����6�C�[�`���3HI�պ�Rq���sAA��ǎ�_�Q�TC���y� ��O�̚l�׋�#�b�΄��S{�����H3�	�R詉�ɫ�&7�"Z$�	;�k�� �'u���@��D�p�&{��1�����`��q�8o��;��O#��P�;翯ɸ�D7�e&�xl�=0�,U���$�V��Q8ה��C��|.�5s��@s<�*� �ܼ�摘f�k�҃���u�.�<FOP�nY�K�1Pt�4.̂��rX�����2��A;��ø��/B�P�&.%����z�}cQ����6V{$���G!;�6���&f�R6a�aRw�t����|�{@�)>�4� �)���j}>^^О�mD�ֵYA�r�"��@���ZND����	�f��l����x.sc�'!J��*���
�z�d�\�����qȠ�*����[�\J+T�E��?���r���|����7�'�d���a�jAP��y7������p�(��1=��6�3/Q֨/sڬ���y����J����&���Lf��uhy`����'����K��������z�SE^'�[Dg�My��(n���&�ఄ!�w������_�����1R�N2�~FM�,43${��D��_����W���1n֜bctG���~D}u5F���v�(��"aI�33333}�^�|Ƃ��>Ma�b��Dbd�E?�;��̴jjE��?_�sl�؃��jFVg�4Ŕ�<�����Qn_mt�o�]l��w�$�̚�;9�#�X*p�)2�P]<��UQ-fffffgOթ�����$�N����0�<^o�{~"�x�ݜ/����Ъ���	qo+r��g[��:���d'��,G����>Ȼǯx0�=3�ʜx� ��}PD�L��!�L!`AQF0=�!P�(��dA1E""I�T��=�� ��{�ZN��u�Y�-�rI8] Z�)��)P���5Ԧ1�f��^v����.,T�>���\dp��Xʅ�a�*^I�K���I")+�߹{i�qԙu1hH� ��:�l�8�#<�ΖČb�$K���:�5�,�I#��J�7�7(2��)ɫ{("e
�9�yO �������e��~3g�8C�;�H����Ԋ� � ��9�s�F��+���!���DK�4$0Q�V��t��J���5���jD�$��5,f&T�$�R�@Y-7&�IVL�j�(�(��;<�%�ď��fQBQ���B���ډVt�ƙ&^�L)�$JD�#&免�j���L{@�.4dh%7��Y���#f��\hm��))�NEE�P�v��������=��4���:@(�C�I�`4/[V��z?<���?�3��{���S3��1��nmA�������Z�m�%)0)+�J
���M�k�jj,b0h2�f��Q4�"ˤ�ex�ҍ�q�p�c��+B<0Pjn#�e�O(���ۈ��0'c��+�X(�YF�b�B%h_A��bɸ�V���QN0N�iK,z1��£b@9"�2̕d�1�A��\:4[U�n�&T����`P�&M�N��h[�C��RDV��3VY�
Z�%�9(t���P�lQ��d�S�y��~�� �,���T0Te$N���;`�ǈ�$��@A�F$b�=����A� ����n��7wt�71�����_C�������^�>P���i�������
���Y
�U�raF�LX����K�
��Jz��	26oY$�xN�(�_%+Yc!P�(C=��*��L'��$�j7,�	�TJU��Q%3��4`��� ��!3�5-�=S�����w��Z:����_ QQ�gL��h���sq�Է�u�@� GN˴i[��F�j8N~z[�n�Fng���h�a��f$����̊�qZ-%;�x�z�E[n��m�&#Q���E��"��O�9���@�,�ot��-�g���,_]�j�ޠRlCz#&i\y��rQ؇�<��o��C5�ER��ǘ9��!�A�Z:Z����UF�q���A�`�<x��܁������^y��j�HǀY�vӆTw[��Xs�g��O�T;ϯ��.�a�X%A椵n䀩9�:�~o�]��1p���<̇���$��
 8����u4o� g�q`:� ���x�`Z��v3��Ii�K�W$K�4�F7A��@ 0 �4A ����2� L/���V���C6.%ִ_�EnVڰ�����F��Iy����~�u��Eя��厧'�o��U�B*�$@����G~EO��8��t��}�9��m�v(Tq0f�&�ZL���z�Y�L�4���X���|���k���j�J�d��Z���S+'�_40$�q�j&B�D��)9�k���K�U�8c8<o�K^�%C��D����8�b�h��_��#S���Y��&H�%S�R�ϥ��P�4�:�ʨ�]�x�<�	�Gq�e�yn衰]�����l���#�A�����C�0�HQ,C`IE��!!�0Ȥ��L�TL��V���9n�+*�E��`��	$A"�b�"�!�T�`b7*��^�`QqI=�Xy�	I4�R<PFY�c	�!��߮*�&�.��s���&祅Kq�dZ렞`���%�a�2���v*���9KQ���=��ق^@!AdD H�����q�=�I-R�(mB��	`,��
�|�r��Y��ݶ�	���ࢢ�� x�����qX��k��% [3)��T���66�-��"��2o6˜)U�0������d�օ�3 <�FLHLۍC^	�{�N�>e��t!�x��5{�29�!�� � |ۀ�)@<��������������!�VD&��Q��B�,�ņ7Lɰ�c��f�ȹ�bz���y��p��s���-�,sX�%!	�y�v���l$Hlj66��(b��.m�P�f����ƕe	*�j*��Fk}^#��TJ*4Tt=�cI��k1�E�1#q��o�p�	�.�σF@a�Oy��l!��y�y<zi�K�wq%�s7�v ����;:�*8�In�Aa̠W �
y�A>@QMu��
bj/	e+���D�ty�F�9H�b�}{��1 2�SB�x�;.u��ᬏgܜ_'��1f/��z���qй^�IA�����F+w^�l4hM�N�?���ۍ�j}��M;�����}~�������"{ �łA�5�1�'z{08��;;�J#I�!��ܔ��@ޞ��Y��S#�4�$$� H':C}�2f^Á�~:*�n����JOg/�9ؐ⚚.л�;c��'�sRΖ̒�����,!��P��!?O3~ƒ'��Yݮ��5g��@�L �Dʄ���v����Ze�/b^�XB��<���ZF�O��
�Q�<�Hб��ʨ+mlO�9?ܬ����8��+�t�0vĨ	ha�y���g��(S���u,�h���F;D��>�X����X4Ccw�UC#�҆,p�*�A�!�Q��yϼ�/U����\��"eBur �s㸉šT�i���>��~�J
�k��`wA���b�-�j�g�@̐۲ȁ�	+DG/Cڷ��≮x��;$�P0�{��z�O��`�D|.���-�<����"����:�P5��Ö�n�u�_�X=��<^�?kȼ5��-�oE���H'��~В�ĆR@�8pquAk���$a`X_g�w���2� �9+� �A ��ķ�;z5S&�H��S�n(拷�$5�$ffq"�N���Җ�$�:4��9�N_=�
���s���OlKD�{���H<�ME�ډ�aj������7|�4���d�)JR�-a�a��`�fr��/&�����TI��1 �
<G|6e��ho-�!(�j��̂W'T���mB�LD�A<�"��}O���� �L�%�w'$���p�MYy�3��'_^�`E�!��R]���yq|��¼o]����*��R���DgUU��Z̒I-M�$�Ԣ9D�R/��M�{������V�S�Y�H���6�X�Q5���6�0��F�;9�gK�.Js�89�)J[Fĥ)JR�Sɪa��JCE$��qV�8�m�AY �]Z������fy�X�&%�Y�H']��ܚ�lꪻ��Hl�lw3?�v��M�X���2��� ���]{3�)(fw{7�_s�Tl��z����$!x�d��X=�x;�YL�`ɮQ��BR6ͫ���]L�(ۡ�]�`��fu4�`8��~�]3%���6A-�0����0@;�*A�%^������?F%�QԞ'�i��<�~u�Rط�]Q0k��vD0�U|p�1d��9N{��q�̍M��5β�p1�Sɛ=E�vx�8����< ���s���)H��=�V����gJ�v7�̕p�u���QZr=��d�.�	1 ��@KZyb`=P%��3dk�
��8��)������B�t����&19�����L��	$���)q��N3��/K��rw6��ٸ�Ж����1�	�L�UCf��P�,@�	%.��nE\�SCF1�<{�G�.�M��צ����-��~���&`&	}�]~x��=�(�Q[vWTVt|0|��Ȕ�f�c�\�p���)��J�H�}�����tdvn�$͜k]7\��A�
T���S�P�x
��E�IȂ�
<����G������'�`x8�vO����	z�� E�b�G�����UL�N��:ina�S�<��?�&�Y��<Z��[�*{�}�=G��R��ڼ�g��/�xHi:�aA�%ٌJ��2��DFp���^����8�����Ɇ`n5
����t9�_UC�2�t�wd�w:�k$H`��2� }���H��(L�3y(|���j!�S��{_�z���Ȳ��L]$��}f�GF���~�Λ$�u�y��ɲ�����j^��������/gId1{���mw{���e
d���83Dg�A��b>��Nt�e,��S�TTH��a�i�>#�*`�2Q��@Dp�,���P?ݦ"��$�Ef �Aw��d��[IW
5�7ĩ��7�H�ߢ<
c$bpH"�12%YJ��(!���+��^!B�@���=�{�X?K.6�+���͢��<3 S��ՙ����@��=e����f�ʹ��_�9��ҜK���qV��H�9���h�������H�w��}\R�2���v�����L��ܗq+� ��AgL��́h�E�ft̜9��OH �+Z�s�1��c. �ʣx͌_�x�ʇ�=lY͸�8�x���OCjIy�P���X�!� F��H�"�B!۝���>܇�r��3 ���r@N���"�22)'�a
"Ŋ,QI�
�/�ƜB��:h�o�@���o���B��x�����@����*Px���O�^�5�=�8�z�J��	��T���j����NP�1���z���}�m���n�0��Ҙ0�)<~=����F
0DA�DE`2 ���PA"�TVR�� �@U�����K�ȝn���!F��;�&<fy�T$K�=M���'��<�A� o*�qKy� �}�:ჀZ�G�<!��W'	�+�P@X��E!�=
1��ܛ/p�PA$�B�檱�U`������/\t8�m��0(=ᨇ��`��IdMIEI�E�{��{ɳi$�M��GS0w�y�x�2�
#�P0α$B�)���0bb�����m���9Fs���7���6	�ܦ���7�l����d�6p$��q��|d�S	 EQ8�f�"���	'k�cG��}G���@}��;���&�	r���}(�������=��P�	�6��H���V��y�r$� 4�!�M�ƎQ�ށ���$M�%#0�p`����9HB3��p��{8(���T��� ��
N2�q٤m���E"+��GaBq9�C��5�ڹr�&'������߿��~_��!K[��Dy'Q��Dx���0���Y�T��d�B,B����ZO�j<���������G�������ȍ�+�� W
<!��&:�;�0�C`	�v�E���r������1Uȳ�q�4$�!��izE�]����)T�k�@���bsv8>'Ч�܆+�b1W�Z�`Q�;p�:�Bֱ(��TJl�E�!�� ��w�uT_1]���ӗ;����D
���np������@TDT1C��'�)�AAd�����h��B��B�C� ��H�� r;��1b)�U;pn�B���f@z�!�p�#��Ψ6�l{��IB���� 0;Gd�v}�֖��3�C;�i��yD<1B���[��� @*��1[�c��i߈u�݊X�NV���2�,��				�}���@Nͻˢ^I0���އ���F�:T�B�F���i�9К�@�!p��af8�Yq�l�(X������~�C�Ϙ�J*�����hf��_� ���15@��l�;���u�����E*P:A1��bh���n09I2�S�`�4 x���oYj�ń)�Pjg�B�J#h,'��=Ym|��C��X� ��N[*HҘbc1+��%����X%�P���
ԃ�JA��kv95bFH9�Z$�5���K��'Wq9lG-o�)�<7��V�m��m@m��m�W6��Hh��cj"�X�;�Y�X�����!Μ�
`	��T�=��2�hv�i$��2D��U;ˀܣ�u�2Aڱ?�9��p��!C�i���d��8+�d9K� r("2	�W�^TWp��8u}\��H�N�'�z��٣��&�,B��#1ew��V\�Im�#u�N�%�H���@6e�	K�$��R��JD�!��aō�Qa����y����٭|*�=�Q��!�q'^+4`ݒ��d�oV)�r@�n;ɤ�9l�|�:�	�����b(j@N2dH$�qR�M r�`Y�l�էOF��ی� ��12j79�fl�e��� �uZ�� ���,Hv�brN��j���+����3�aB�F!�a#%o������L䤣d5���l��)f������_Y�y��	�atAh��KF�{䘃D ���tNi1�f`F*� ��1��^�3��$�H�����9���?eC��0	�0�&����$�nD�LL����(����. :�� ˊ�.��Ȩx&,d9�$	qe ��3�5� 4)��9Ѕ��2(}&q9�'i
 L@�9���T��Ԇ�E�w���k&���Q��Q4�@ր<�".�d���g�0@����?$�46.Qo��,+��q(���1�@���19�C%�Q�����#��e���A��E�I�b&�cp�A�]�,�þPf��\R�~0]Y��Ѕ	<@�.C������.��#N�����9�h�� &�f����1H��X��I��g���p�V���^�$��1"{H��`]&v�v��)H���a��(i������LB�\O�`������۠��ulV3��������Jh����øy�"""""""#���������������ֻ�d����(���;z���>�m
H{�����\G-�@�Bð���a�����m��m��m��TJ��{qZ m.[�X:�J����4�ڞ�Z�e�����,�����|@t"i�JSY�4�X-1���P�ؖZ~����y�)TP&r ��2O�h?,E��Nwa�R�ۄ�� �*�� }�'z���-�MB�����P����l��5�,������� �"]��*�{F	��sbq���� ��C��B@��D UUP0�*��I�
�ql���RT̉BH�#1���)��Ɓn}讑5
��F�2ǔ:�ãxp? �<�Ma�>A��D! H���ݜ�Y�2&��8!H�	5����J�p��s�5W�Y�Ύ��܇�:L��
8��X$�2 F$b>�PHă�:K�j�q�}��Y���Q���|F	�� ���Ј<�6�wAv��R޸R��K�E�h2B�0fJq�P��Zϓ��x�W�APѴw����_�$�*^`F�.Wb+ϧd"XD�	�HD�����j~B`62蝶$%���]�-&�9B�[5�@�!��/�p<����.D<r����Z�h5MhQE7�p� z!<H�T��3� `3�nzN���|����+�$����A�t%X� $����H�Υ���m�P;�YE�Yq?�<�h����;�+��5��f^�wK\P���qgl3��a"�S���p�����r�{<TP����Q8K�!����N�X��Ӻ ���|!LtLqruZ�a�D+`�9�2lH�i`P��!-I���Q}��>E���L
X�*�i{N��
J���2�pߝȣ���z�Ķ���[�P����M��� ��U�aM�{��E�*�
qCk��G1c���H�9�:�^ f,ިhr7��:��
H. n(,�41"�Kr�IH:�m�t!P�v�֚������:��b�y�;`"��$�D�'x���.1!Y \Y3lT�n7Њ߱9�M�o <
�؁$��Yhy�%)u�\,KzD"�,�R,�~[QW�K�jSi`r�Y� �A�TN�>q��f�˜U�r^�1m�[M�!�Buu{.=]$�b�jR.�|�E<]
Kr��Np�܀�Q`w҂��w��Q
�X��"@�d7��}�$d�Q�����-�����v��9������{7�jr��z�j;L�Np<͂r�i����np���Su����V� A��
X`� hP.�2�ACflh�#"C�!�3�I5�wbrFef X-s@������HM�ʖ0' �@����/[��v�%�hp�e"S9R,Pv�`5�W9��BVy���k�,�h�<�[� yS`��J(�b��-�Jn
1�P�����X��gq��?��~߇wX��(&�n��YpR�|s}��Pn��)����<����QV)5�!$�w.�C�e�) H�)� C
�7���Xp[�'�z~נ��3��y#T�-������"��F�O4�Z�A�`�c�B-8Zc��!@�<m�l���ײ�!�:�	�!�!�	p=��z�)A0LAM�����G��֟^C=z���[:!�@ةu�z�
�@ ��lEhU��v8ma��s�{�y�&f	� ,�{�;��Bd{�!U��÷�c�m�	cbbx�Q��mU|�;.����S��H�9�}E���$N�?�43� 4w�#�X~�`���[��~�rC�R]e�� j�@$w@>! �	�ES% !�	�������� �x�p�[���4C[�y���j�Z�%�l@MAlԡ`��	cB^E7q�46R%��Q��3cY& X}��>�W�I�y��Y���2��BDy��'�=v�6�!�Ђ���g�%�Z#�:��9��.	���ԎY uu"����xx�)~��Ik�[ar�R�	��1�p=G��1[˩[&$@�ɃW�Q
�	�n��玙�h3Xp���������n�ZR�e���7��#5)�h0�e,�Tk�G��V"1M��F-���t�����ݮ�N'�`Ӱ<��,XB��:�����.���9�p�tW#�6��'g�*���c���l�Ű�.�3Y&%Qq.�ŋ�Yx� 1c�%�jqLD��;�b�r�����K���tq�b�) u6\�="i&���މ��I� �SwDf++���s�	��6>j���7����6/��]��:AY�*,�a��|]H��h���r���e�_�R��P7jԄ��+�l X223�o�mj��z�X�� H-wc�FXDFs��Wˁ�,��](g.��6枱i�OZ�?�:*�����Ɏ����q=��s&Ìx��ܗ�:X�z� ��Qn�AF��  �P�*9�8��/ 7C_ 7�hh���)60�5��=~�S��a��UIjB�\�]�v���� �)�<5eٴ��ރ۠�J���ntD����,���sXcf����!�����0`�<`(�"'�[x�pi����L�g�B��� 8�y��ڀ�0�0��iu�B�@\�)cWfAt���I��ǡ�$�5���������7�k�i{���`a��!1qL�.����zk��XY�)`+�)��0ŀȌ�����+T0�~ͰbIi`ZR��6I�B��&��q7m�UUUUX(YLȶd��~�y�7T�4� ��m�N��%"Hވ ��BD�0,�h@е<����5P�|@s(Y@�B���.(m3�@�@�cI�.Wpؚ�~<���⹨�ti��Q�؃D،&���&H�ê.�6���#֍j�w���=��$޾�����%6���9g��08Da�!!��P��/)��F$��)m �pjl k�r�՚)ૉY� i�\���q���� u	p���P���m��pS�!�Q7�%%��2n`Db"#�� �� �S!p]��J�%���R�.-�4�����m��u�󻡓��r�3!���!De����	�� j�=�t}�I��gJ
�
�Q�d�&(`Bh�0���r� �w��H,�h�6�%$��.aB��n#�N��¡%7λ�2�[4)7��b��&Nc��!�l �($Et1LED �r��$�m�(C 
BK��.�d4-�LF��1d�T��`�����pT�w4�eA���(MFb�,�E�&H�] 3^2DU�>�p�0;�!����� ,�^��'ǡs�V#mjR�
���0��R��6�K���[���������h��q�,0, m�ݐ�ND:1W8��K�9�&��G�)"�Ȉo+c>�ܢ<����˘�k�-�A#�0;u�s
#��&����N�f�_;D�`;@������^R�C�y��P����bn5;�ց��40�͠GĹAd�$ �</ި�����Q7[g\��!��FDA�����[Y���|�b]�p���Qו� `�����+�*゚�6�ŀZ* %SG�F^޴x��=h}׊�тs>�'�j/B8�����Eͦ���>�Q�����*��"Pw�')��� t��Zo���Xű;�ߡ.X��}������i�v�;k7�l,��� >F����cCc;Q�Ԡt�Po`!dA�sH�D���_��	�M\�O/#��9B� O��y����m=�p�4�锈I�&�$l���T>�?�<
 �#D�'�]ܘ5ՕU��&%�c��M[��b�opa]�a��֨$^��KkH�"��4�PÐ���p����l�3��^H&bf=��G�e��>���sÁ 2E!c2A�BŜ���?�yҟV��*�_JXZpOp�0���X%T&B�P�Kr��j�M6ؒ�H�L�^��`׀@O��"Dđ?�cG0��z �~��k��������.�d%O$(�!9�E��A��GR�P&�^��I��,;/������ � ����=~���/>%�a!���!O��� [-����B�������"�@���l{Eܷb�	N `2yc>_E:,��5!��4_.����N�D�n^��in��-��`{���ԡ�E���*󫉑LY L���Ń(H#E��%�Nn2`B�@
�.#���I�gd��v$S�yU"pQ�˙P<�2$V���@�Gn�@w�Ua�`�uǳ�rI&'�l�i�j4����z'ğl�1�Y�t��gܒ�R�8(VB����T>8�u�e��,�b+���
"��p� �	��#"!$_��t�_�=!G��V�<�I�#�A��%��˦�o>�KM�쑶H��+#ө*�e��]�"b-�`����z����w�{��l���A�80�Ѵ6�����6�~����uH�>��q+67��}�谂�nz�H��*E�)�3:�^QC�~�zhD�H�$B���X)��2~ߵT�2����L͇� ��0�
�ѳ�x��Je��	�i�t)dRE�B�S���Z���I�q�?p��2�*	�B�6ŧ�Y�)�V�!~Б��o����o߅O�Q§��~��'$�zz��o
���7+f�!h��uC�!p�P=@x@"	G�C�aKs�u���*�h~���E��u��2��ٸ�v�'���r�(e���:K����DC�,BpgM � ���(����x��w��.���?�-�{+�q> @>� х��+f�],���iԣ��GRe��	�w�����u	A�P�Gt5�H��J��4S�l۽�����/��@J,V������Đ���d�T� B-h�w�K'��	D��yR�E�J����b7�2@���jH�����q֛:ۗ���`H�H�I�/�BT�}H5�!()��$RoH"��z��jv� �0H �M���P��ċ��ÿ��VB�'���	B��u"�/�*��n����?s.I�0V���H �C�Q
��L�"���!��`Չ�K�R+�H\z�E���0��g�٥j�M�H jW��1�����@�U�
P�N*�4H5 (���k$Ƌ�$R�#z1D"$X�pȽ~���@��Чǿ|�]�0�*kքR`��L�*)[@��,�>��i��A��&�4���Pd6aAxg��·陁 WR�^H�L6��B��Z�Z:�!��� f�ʢo$Z=C�>q%�6���#P(ᎈ�sW��|HJ�ijN��@�Ѹ�-lj���x{#��NU�����J�mYrd�P�`k)2`�'�l�;h	a2�vӑm�03�{Ġ��~&��
86(z��R6��"���Z`���2P�H�Z �!AcW�� �����8�w�&e�����;��m���r����c���I�-E8����H$�d��ͽ(q�ō@�p�/�%�B�a�OD:*�/C��Uv��h@h�9�]V�PR(f����X��YM��7����3�z'��R�P@5"|@�]I3��$B|�1���;H��ǭ{-&�A(^��2�� ���pxK��wd��mfA��W���z�	�,��(�m�ӝ/3.�i 8�0�,X�J��#�0Ts�X8<�B$�(�s�ν�4UHko\ ]�Xt�_3�S����Q ЯX,�!t�ʵ�Z��`��	I.b��,О������L<��i��{�	"A��  ��������i<�Mz{��`V X ��!zk��|@=I�ڀ��"@��H��M +�$9�]���q��lyd�AU@���)�
J�Q$D����?��ܞ�tٙF�95�s�}$~sȾo�7�6o���15>���o���'���F��^�hG�:A?+��<�y*a��1�ެs'��I�r�����:P��$�4��0�����<�{�L1�4'	�dB��"V�O,�c4f�� ;i�4�'@	�#�z�ܜdg��4��W�Y���8v���i��8��wI�1���I����> `՜U��:��SDx��L,�.�aSM�h$���nN~��4�a �d�3xv��GU*b;x��!�(�Q�C�!b�KO�$X�f#�0-�*k��f5lj����P��(�C���x�
s���,-��|<5��-1h�������\�fk�ф]�z� r��뜬>�%~�`wU���I1\Bv+p���Fk������;{K�V9x��<x8q����͛62�!����,[������q�����\̒Y�H9L�pVQ�nփ]��/��S:�	*n�ۣS
�C!
~T�$�y!�����s9��Ã�_�*��Rִ�HL���#'��*f��@�͌�(�-m|���U�׊?���V���6|!5�9t�GQ�!,SH)����~�M題������MP��F�����}-8��{��ݸ}|���>�c<�x����ϗ����vc�Q�$�����ΐ����w3���b�2C����x�sh.Awri�sF�>u
eae�Y>ٕQdhD��,1�p�Z�p���0kٛ��{�fW+s|ƞ��
]���6��t��
�mT0�bҨ�3Q���b���?�VA��D
�_�a����A��c���7
�u�Q���Ԩ]̗I���=$��|;�tY�6�7�� �B����㎣��bؙ����4�ɿ�2�3���̡�#�dRH�,�m��>R{2�y��0�"L�UUV��	�|���{�ؔ�۳�S��tݘ-�P��؈�<�Y���h�%\���F��;�~�����b��PUz�`>r�榭�.�߆J��<�#A�n&Z�]1����[|����A3����	T�Ɲ\�5SqH�OE�]T���,+*�Vend��v��g:�j�Y� $2TJL�CRd�
�NT5Ay��a
6t�#����&f��8����b�Kzb��V�s�F��M���g�(!p���l��Z��܄:�.�<�x��G��fc�h�c)
��:��I��V'$�s6[mx��)^��p;j�v: k��2V7���=�Y�%=IH�_8p(���G�k4���m(i�\puq_4�t(O��g�3th{Ν� �u�j zV5����V�R�j����VVr|/�`)b��m�0� ���3����<[w���֜�fnIxC��S�5N#-k��#"��V3�p�j���zɢ.�%z�$�F~P��e�G0�=43g	[�k���	�r:�\��u�,1���D�3feھoQۅpػ�7BP�:�k��p��8����������"L��1��Ŋ���o�B��~ȧ���Y0�(
�ߛ6>����9�(0�02*�S��c�p�o�6�#?I�* X�Q�����>���C��;�Ɏ��|�D+�1��8J�F�R����K��L]+�f#�>�c�c�`��q�Q⟥ކ�@�0ř��?v�ѩ{BPڧ\��X�r#L����s��9��0;�'G@��4���N��c �w.�yo�o�	���%���kW��K/	���②��z��U>��A�g��z4�ǂƗ�K�]j�$�;�֣U�Ƀ��Wo�HBݹ��p�+챘�<�n�0���d�|�Q���?XN�t��=t���$���4ET��������of���<�]��Rd��@в�qc��H�Q=�bW����?�������ܑN$�=��